package com.ServiceDesk.ServiceDesk.controller;

import com.ServiceDesk.ServiceDesk.exceptions.TicketNotFoundException;
import com.ServiceDesk.ServiceDesk.model.Ticket;
import com.ServiceDesk.ServiceDesk.repository.TicketRepository;
import com.ServiceDesk.ServiceDesk.service.TicketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketControllerTest {

    @InjectMocks
    TicketController ticketController;

    @Mock
    TicketService ticketService;

    @Test
    public void testSaveTicket(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Ticket ticket = new Ticket();
        ticket.setTitle("Energia");
        ticket.setDescriptionOfAProblem("Service Desk for Energia");
        ticket.setEmail("Tauri.Ruut@gmail.com");

        Mockito.lenient().when(ticketService.save(any(Ticket.class))).thenReturn(ticket);

        ResponseEntity<Ticket> responseEntity = ticketController.save(ticket);
        assertEquals(409, responseEntity.getStatusCodeValue());
    }
    @Autowired
    TicketRepository ticketRepository;

    @Test
    public  void testDeleteTicket() throws TicketNotFoundException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Ticket ticket = new Ticket();
        ticket.setEmail("tauri.ruut@gmail.com");
        ticket.setTitle("For Energia");
        ticket.setDescriptionOfAProblem("Something is broken in this application");

        when(ticketService.delete(1L)).thenReturn(String.valueOf(ticket));
        ResponseEntity<Ticket> responseEntity = ticketController.delete(ticket.getId());

        assertEquals(409, responseEntity.getStatusCodeValue());

    }

    @Test
    public void testAddTicket(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        Ticket ticket = new Ticket();
        ticket.setTitle("Energia");
        ticket.setDescriptionOfAProblem("Service Desk for Energia");
        ticket.setEmail("Tauri.Ruut@gmail.com");

        Mockito.lenient().when(ticketService.add(any(Ticket.class))).thenReturn(ticket);

        ResponseEntity<Ticket> responseEntity = ticketController.save(ticket);
        assertEquals(409, responseEntity.getStatusCodeValue());
    }
}
