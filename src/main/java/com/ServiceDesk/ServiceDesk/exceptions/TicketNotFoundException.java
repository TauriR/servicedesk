package com.ServiceDesk.ServiceDesk.exceptions;

public class TicketNotFoundException extends Throwable {
    public TicketNotFoundException(String msg){
        System.out.println(msg);
    }
}
