package com.ServiceDesk.ServiceDesk.model;

import com.sun.istack.NotNull;
import javax.persistence.*;

@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, length = 50)
    private String title;
    @NotNull
    private String email;
    @NotNull
    private String descriptionOfAProblem;
    @Enumerated(EnumType.STRING)
    private TicketPriority priority;


    public Ticket(){
    }

    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescriptionOfAProblem() {
        return descriptionOfAProblem;
    }

    public void setDescriptionOfAProblem(String descriptionOfAProblem) {
        this.descriptionOfAProblem = descriptionOfAProblem;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", descriptionOfAProblem='" + descriptionOfAProblem + '\'' +
                '}';
    }
}
