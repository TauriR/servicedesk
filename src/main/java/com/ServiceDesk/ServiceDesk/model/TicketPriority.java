package com.ServiceDesk.ServiceDesk.model;

public enum TicketPriority {
    zero, one, two, three, four
}
