package com.ServiceDesk.ServiceDesk.controller;
import com.ServiceDesk.ServiceDesk.exceptions.TicketNotFoundException;
import com.ServiceDesk.ServiceDesk.model.Ticket;
import com.ServiceDesk.ServiceDesk.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/ticket")
@RestController
public class TicketController {
    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Ticket> save(@RequestBody Ticket ticket){
        Ticket savedTicket = ticketService.save(ticket);
        if(savedTicket.getId() == null){
            return new ResponseEntity("Ticket does not exist", HttpStatus.CONFLICT);
        }else{
            return new ResponseEntity(savedTicket, HttpStatus.OK);
        }

//    @PostMapping(value = "/save")
//    public ResponseEntity<Ticket> save(@RequestBody Ticket ticket){
//        if(ticketService.findById(ticket.getId()) == null){
//            Ticket savedTicket = TicketService.save(ticket);
//            if(savedTicket.getId() == null){
//                return new ResponseEntity("Ticket already existed", HttpStatus.CONFLICT);
//            }else{
//                return new ResponseEntity(savedTicket, HttpStatus.CREATED);
//            }
//        }else{
//            return new ResponseEntity("ID already existed", HttpStatus.CONFLICT);
//        }
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Ticket> delete(@PathVariable("id") Long id) throws TicketNotFoundException {
        if (ticketService.findById(id)==null){
            throw new TicketNotFoundException("Ticket with id: " + "not found");
        }
            return new ResponseEntity(ticketService.delete(id), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Ticket> update(@PathVariable("id") Long id, @RequestBody Ticket ticket){
        Ticket ticket1 = ticketService.updateTicket(id, ticket);
        return new ResponseEntity(ticket1, HttpStatus.OK);
    }
    @PostMapping(value = "/add")
    public Ticket addTicket(@RequestBody Ticket ticket){
        return ticketService.add(ticket);
    }

}

