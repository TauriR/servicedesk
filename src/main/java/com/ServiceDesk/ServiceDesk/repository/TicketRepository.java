package com.ServiceDesk.ServiceDesk.repository;
import com.ServiceDesk.ServiceDesk.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long > {
}


