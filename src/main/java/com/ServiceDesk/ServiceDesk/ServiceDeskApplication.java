package com.ServiceDesk.ServiceDesk;

import com.ServiceDesk.ServiceDesk.model.Ticket;
import com.ServiceDesk.ServiceDesk.model.TicketPriority;
import com.ServiceDesk.ServiceDesk.repository.TicketRepository;
import com.ServiceDesk.ServiceDesk.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan({"com.ServiceDesk.ServiceDesk.service", "com.ServiceDesk.ServiceDesk.repository"})
@EnableSwagger2
public class ServiceDeskApplication implements CommandLineRunner {

	@Bean
	public Docket taskApi(){
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).build();
	}

	private final TicketRepository ticketRepository;

	@Autowired
	public ServiceDeskApplication(TicketService ticketService, TicketRepository ticketRepository) {
		this.ticketRepository = ticketRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(ServiceDeskApplication.class, args);
	}



	@Override
	public void run(String... args) {
		Ticket newTicket = new Ticket();
		newTicket.setEmail("Tauri.ruut@Gmail.com");
		newTicket.setDescriptionOfAProblem("I have to finish this task");
		newTicket.setTitle("Service desk application");
		newTicket.setPriority(TicketPriority.two);

		System.out.println(ticketRepository.save(newTicket));
	}


}

