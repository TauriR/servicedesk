package com.ServiceDesk.ServiceDesk.service.impl;

import com.ServiceDesk.ServiceDesk.model.Ticket;
import com.ServiceDesk.ServiceDesk.repository.TicketRepository;
import com.ServiceDesk.ServiceDesk.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TicketServiceImpl implements TicketService {
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Ticket add(Ticket ticket) {
        Ticket savedTicket = ticketRepository.save(ticket);
        return savedTicket;
    }

    @Override
    public Ticket updateTicket(Long id, Ticket ticket) {
        Ticket ticket1 = ticketRepository.findById(id).get();
        ticket1.setTitle(ticket.getTitle());
        ticket1.setDescriptionOfAProblem(ticket.getDescriptionOfAProblem());
        ticket1.setEmail(ticket.getEmail());
        return ticketRepository.save(ticket1);
    }

    @Override
    public Ticket findById(Long id) {
        return ticketRepository.findById(id).get();
    }

    @Override
    public Ticket save(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    @Override
    public String delete(Long id) {
        ticketRepository.deleteById(id);
        return "Deleted successfully";
    }
}
