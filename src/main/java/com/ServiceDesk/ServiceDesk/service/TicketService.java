package com.ServiceDesk.ServiceDesk.service;

import com.ServiceDesk.ServiceDesk.model.Ticket;

public interface TicketService {

    Ticket add(Ticket ticket);
    Ticket updateTicket(Long id, Ticket ticket);
    Ticket findById(Long id);
    Ticket save(Ticket ticket);
    String delete(Long id);
}

